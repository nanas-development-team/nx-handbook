# Strategi __Branch__

## Daftar __Branch__

1. main
2. dev
3. xx-initial (cth: nomor isu 30 nama Victor Chris, 30-vc)
4. common-nama-fitur (opsional, cth: fitur search, common-search)

### Aturan Main

1. Isu adalah single-source-of-truth
2. Semua __branch__ harus berdasarkan isu, tanpa isu berarti __branch__ tidak sah
3. Jika tidak menemukan isu maka buat secara mandiri dan lapor
4. Pembuatan isu harus berdasarkan `main branch`
5. `dev branch` hanya digunakan untuk menggelar (deploy) ke __staging__ secara otomatis
6. Sangat tidak diperbolehkan untuk membuat __branch__ berdasarkan `dev branch`
7. Jika sudah selesai dengan fitur maka lakukan MR (Merge Request) ke `main branch`
8. Secara kondisional, boleh membuat __branch__ berdasarkan isu terakhir dengan catatan isu berikutnya yang akan dikerjakan harus berkaitan dengan isu sebelumnya dan isu sebelumnya masih dalam tahap peninjauan. Contoh: isu A membuat antar-muka, isu B membuat fungsi dari antar-muka.


## Diagram

![Branching Strategy](assets/img/branching-strategy.jpg)
Strategi __Branch__

## Tata Cara

1. Buat __branch__ baru berdasarkan `main branch`
2. Buat MR dan buat daftar cek apa yang akan dikerjakan. Beri judul awalan `WIP` atau `Draft`
3. Siapkan pesan `commit` di awal sebelum mengerjakan apapun
4. Wajib hukumnya untuk mendorong seluruh pekerjaan setiap hari meskipun pekerjaan belum selesai
5. Centang daftar yang sudah dibuat sebelumnya di MR yang sudah dibuat
6. Jika sudah selesai, ubah judul MR dari `WIP` menjadi `Resolve` kemudian minta bantuan teman yang lain untuk meninjau kode

![MR WIP](assets/img/mr-wip.png)
MR WIP

![MR Resolve](assets/img/mr-resolve.png)
MR Resolve